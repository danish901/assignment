# ASSIGNMENT: 
# creating class
class Conditional:
    
    # defining method
    def check(self, n):
        if int == type(n):
            if n % 2 != 0:
                return "Just odd"
            elif n % 2 == 0 and 2 <= n <= 5:
                return "Even and between [2-5]"
            elif n % 2 == 0 and 6 <= n <= 20:
                return "Even and between [6-20]"
            elif n % 2 == 0 and n > 20:
                return "Even and greater than 20"
        else:
            raise TypeError

    def greet_bye(self):
        return "GOODBYE. Have a great day."


# creating an object of the class Conditional-
# con = Conditional()
# res = con.check(2)
# print(res)
